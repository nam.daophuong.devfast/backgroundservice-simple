﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BackgroundService_Simple.Models
{
    public class MoneyModel
    {
        [Key]
        public int id { get; set; }
        public int money { get; set; }
        public int status { get; set; }
    }
}

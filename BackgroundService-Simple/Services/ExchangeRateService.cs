﻿using BackgroundService_Simple.Database;
using BackgroundService_Simple.IBackgroundService;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace BackgroundService_Simple.Services
{
    public class ExchangeRateService : IExchangeRate
    {
        protected readonly ILogger _logger;
        public ExchangeRateService(ILogger<ExchangeRateService> logger)
        {
            _logger = logger;
        }
        /// <summary>
        /// Excute ExchangeRate background service
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <param name="dbContext"></param>
        /// <returns></returns>
        public async Task DoWork(CancellationToken cancellationToken, ApplicationDbContext dbContext)
        {

            while (!cancellationToken.IsCancellationRequested)
            {

                var moneyList = (from money in dbContext.moneys
                               where (money.status == 0)
                               select money).ToList();
                foreach (var item in moneyList)
                {
                    item.money = 5000;
                    dbContext.Entry(item).State = EntityState.Modified;
                }
                dbContext.SaveChanges();
                _logger.LogInformation(
                    "Auction is going on : {moneyList}", moneyList.Count());
                await Task.Delay(2000, cancellationToken);
            }
        }

    }
}

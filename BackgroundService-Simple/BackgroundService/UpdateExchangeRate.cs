﻿using BackgroundService_Simple.Database;
using BackgroundService_Simple.IBackgroundService;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace BackgroundService_Simple.BackgroundService
{
    public class UpdateExchangeRate : IHostedService
    {
        private readonly IExchangeRate rate;
        private readonly ILogger<UpdateExchangeRate> logger;
        private readonly IServiceProvider _provider;
        public UpdateExchangeRate(ILogger<UpdateExchangeRate> logger, IServiceProvider provider, IExchangeRate rate)
        {
            this.logger = logger;
            _provider = provider;
            this.rate = rate;
        }

        /// <summary>
        /// Start exchange rate background service
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public Task StartAsync(CancellationToken cancellationToken)
        {
                Task.Run(async () =>
                {
                    using (var scope = _provider.CreateScope())
                    {
                        var dbcontxt = scope.ServiceProvider.GetRequiredService<ApplicationDbContext>();
                        await rate.DoWork(cancellationToken, dbcontxt);
                    }
                }, cancellationToken);
            return Task.CompletedTask;
        }

        /// <summary>
        /// Stop exchange rate background service
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public Task StopAsync(CancellationToken cancellationToken)
        {
            logger.LogInformation("This background services has Stopped");
            return Task.CompletedTask;
        }
    }
}


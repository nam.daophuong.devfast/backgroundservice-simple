﻿using BackgroundService_Simple.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace BackgroundService_Simple.IBackgroundService
{
    public interface IExchangeRate
    {
        Task DoWork(CancellationToken cancellationToken, ApplicationDbContext provider);
    }
}
